/*
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	desription: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

db.rooms.insertMany([{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
},
{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
}
])

db.rooms.find(
	{
		price: {
			$gte: 2000
		}
	}
)

db.rooms.find(
	{
		price: {
			$gte: 2000
		},
		rooms_available: {
			$gt: 10
		}
	}
)

db.rooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {rooms_available: 0}
	}
)

db.rooms.updateMany(
	{
		rooms_available: {
			$gt: 0
		},
		isAvailable: false
	},
	{
		$set: {isAvailable: true}
	}
)

db.rooms.deleteMany({rooms_available: 0})

db.rooms.find(
	{
		name: "double"
	},
	{
		name: 1,
		accommodates: 1,
		price: 1,
		description: 1
	}
)